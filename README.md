# process

This repository holds necessary scripts and configurations to run the firmwares collected in [storage-dut](https://gitlab.ub.uni-bielefeld.de/ks-nano/tmu/ecosystem/storage-dut/-/tree/whitelist?ref_type=heads) on an FPGA. These are used by the CI-script of storage-dut which makes this repository part of the whole pipeline. An Overview for this pipeline can be seen here:

![Diagram of pipeline of git repositories.](https://gitlab.ub.uni-bielefeld.de/ks-nano/modular-firmware-suite/-/raw/whitelist/graphics/pipeline.svg?ref_type=heads)

This pipeline implements an environment to run firmware experiments on an FPGA that emulates a given System (currently only the AIRISC Core Complex). All information concerning the other repositories can be taken from their READMEs respectively.

**Table of Contents**

[[_TOC_]]

## Contained Scripts

### `airi5c_usb.cfg`  
This file configures the JTAG interface used to communicate with the AIRISC Core Complex emulated on the FPGA. The script `flash_prog.sh` needs this to connect to the AIRISC via openocd. 

### `fetch_tmu.py`  
This script gets the filepath to the output file as an argument.  
It connects to the universal asynchronous receiver-transmitter (UART) of the emulated AIRISC Core Complex and sends the signal "go" via that interface. The firmware is waiting for this signal after being started on the system (see [this line](https://gitlab.ub.uni-bielefeld.de/ks-nano/modular-firmware-suite/-/blob/whitelist/tmu_hil_airisc_mfs_firmware/main.c?ref_type=heads#L49) in `main.c` of [modular-firmware-suite](https://gitlab.ub.uni-bielefeld.de/ks-nano/modular-firmware-suite/-/tree/whitelist?ref_type=heads)), and starts the experiment after receiving it.  
Afterwards the script `fetch_tmu.py` listens to the UART and prints whatever the firmware sends over it into the provided .raw-file for as long as it doesn't see the marker of a finished experiment ("[HIL] END").  
There are two additional break-conditions for this loop:
- **Max count reached**  
  If the number of read bytes exceeds the defined limit, the readout of the firmware's bus transaction stops because it's suspected to have entered an endless loop. The cause is printed to the end of the .raw-file
- **Abort got none**  
  If no byte is read over the UART within its defined timeout limit (seconds), the readout is stopped as well, and the cause is again written to the end of the .raw-file.
  
### `flash_fpga.sh`  
This script takes a Xilinx bitstream file (.bit) as argument and calls vivado with a series of tcl commands to flash the given system configuration onto the FPGA. It thus sets up the system that shall be used for the experimental firmware. 


### `flash_prog.sh`  
This script takes the name of one of the firmware .elf-files as argument and uses the JTAG interface configured in `airi5c_usb.cfg` to connect to the emulated AIRISC Core Complex via openocd. Then it uses gdb-multiarch to start the given firmware on the system. In the end gdb is closed again, but the firmware remains on the CPU and waits for the 'go' signal from `fetch_tmu`. 


### `runner.sh`  
This script is the one that is used in [storage-dut's CI-script](https://gitlab.ub.uni-bielefeld.de/ks-nano/tmu/ecosystem/storage-dut/-/blob/whitelist/.gitlab-ci.yml?ref_type=heads).  
It takes three command line arguments:
- source directory that holds the firmware (.elf-files) to execute, without trailing '/'
- Xilinx bitstream file (.bit) containing the configured system to emulate on the FPGA (e.g.: [`FPGA_TOP.bit`](https://gitlab.ub.uni-bielefeld.de/ks-nano/tmu/ecosystem/storage-dut/-/blob/whitelist/airisc/32mhz/FPGA_TOP.bit?ref_type=heads) for the AIRISC Core Complex with 32 MHz)
- destination directory for the resulting captures (.raw-files), also without trailing '/'

For each .elf-file in the provided source directory, `runner.sh` calls the shell script `flash_fpga.sh` to set up the FPGA with the provided system bitstream followed by `flash_prog.sh` to run the current firmware binary. The setup of the system is repeated for each file to make sure that there are no unwanted remnants from a prior experimental firmware on the system.  
With the firmware set up, `runner.sh` then starts the python script `fetch_tmu.py` to start the experiment and capture the collected system bus transactions into a .raw-file in the given destination directory.
