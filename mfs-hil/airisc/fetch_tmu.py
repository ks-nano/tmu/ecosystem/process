import argparse
import serial
parser = argparse.ArgumentParser()
parser.add_argument('--output')
args = parser.parse_args()
with serial.Serial('/dev/ttyUSB1', 250000, parity=serial.PARITY_EVEN, timeout=4) as ser:
    ser.write(b'go')
    l = b""
    count = 0
    with open(args.output, 'wb') as out:
        while b"[HIL] END" not in l:
            count += len(l)
            if count%1000==0: 
                print('.', flush=True, end='')
            if count>20000000:
                print("Max count reached")
                out.write(b'\n[fetch_tmu] Max count reached\n')
                break
            l = ser.readline()
            out.write(l)
            #print(l.decode("unicode-escape"), flush=True) #utf-8
            if len(l)==0: # no new line at the end means timeout (we do one extra round if we got a partial line)
                print('Return none')
                out.write(b'\n[fetch_tmu] Abort got none\n')
                break
