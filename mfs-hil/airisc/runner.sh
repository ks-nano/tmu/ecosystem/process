#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
for f in "$1"/*.elf
do
  echo "Working on fpga $2"
  $SCRIPTPATH/flash_fpga.sh $2
  echo "Working on prog $f"
  $SCRIPTPATH/flash_prog.sh $f
  python3 $SCRIPTPATH/fetch_tmu.py --output "$3"/"$(basename -- $f)".raw
done

