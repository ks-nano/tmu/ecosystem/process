#!/bin/bash
cmd="open_hw
connect_hw_server
open_hw_target [lindex [get_hw_targets] 0]

set_property PROGRAM.FILE {$1} [lindex [get_hw_devices] 0]

program_hw_devices [lindex [get_hw_devices] 0]
refresh_hw_device [lindex [get_hw_devices] 0]

quit
"
echo "$cmd" | vivado -mode tcl