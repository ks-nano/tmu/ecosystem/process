SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

openocd -f interface/jlink.cfg -f $SCRIPTPATH/airi5c_usb.cfg&
sleep 1
cmd="set arch riscv:rv32
target extended-remote 127.0.0.1:3333
monitor reset halt
file $1
load
monitor resume
quit
"
echo "$cmd" | gdb-multiarch

kill $!
